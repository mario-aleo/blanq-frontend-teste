https://blanq-frontend-teste.firebaseapp.com/

# blanq-frontend-teste
  - Instalação
  - Execução
  - Build

#### Instalação
- Node (https://nodejs.org)
- Yarn (https://yarnpkg.com/)
```
  $ yarn install
```

#### Execução
```
  $ yarn start
```

#### Build
```
  $ yarn build
```