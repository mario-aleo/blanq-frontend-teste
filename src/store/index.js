import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function validateFetchResponse (response) {
  if (response.status !== 200) {
    throw new Error(`Server Status ${response.status}`)
  }
  return response.json()
}

function _responsible (options = {}) {
  const {
    id,
    entity = 'Contact',
    name = ''
  } = options
  return { id, entity, name }
}

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('token'),
    responsible: JSON.parse(localStorage.getItem('responsible')),
    companyList: [],
    contactList: [],
    companyListLength: 0,
    contactListLength: 0
  },
  getters: {
    TOKEN: state => state.token,
    RESPONSIBLE: state => state.responsible,
    COMPANY_LIST: state => state.companyList,
    CONTACT_LIST: state => state.contactList,
    COMPANY_LIST_LENGTH: state => state.companyListLength,
    CONTACT_LIST_LENGTH: state => state.contactListLength
  },
  mutations: {
    SET_TOKEN: (state, payload) => {
      localStorage.setItem('token', payload)
      state.token = payload
    },
    SET_RESPONSIBLE: (state, payload) => {
      const responsible = _responsible(payload)
      localStorage.setItem('responsible', JSON.stringify(responsible))
      state.responsible = responsible
    },
    SET_COMPANY_LIST: (state, payload) => {
      state.companyList = payload
    },
    SET_CONTACT_LIST: (state, payload) => {
      state.contactList = payload
    },
    SET_COMPANY_LIST_LENGTH: (state, payload) => {
      state.companyListLength = payload
    },
    SET_CONTACT_LIST_LENGTH: (state, payload) => {
      state.contactListLength = payload
    }
  },
  actions: {
    SIGNOUT: (context) => {
      context.commit('SET_TOKEN', '')
      context.commit('SET_RESPONSIBLE', {})
      context.commit('SET_COMPANY_LIST', [])
      context.commit('SET_CONTACT_LIST', [])
      context.commit('SET_COMPANY_LIST_LENGTH', 0)
      context.commit('SET_CONTACT_LIST_LENGTH', 0)
    },
    GET_COMPANY_LIST: (context, payload) => {
      const url = new URL('https://api.moskitcrm.com/v1/companies')
      const params = { sort: 'name', start: payload.start || 0 }
      Object.keys(params)
        .forEach(key => url.searchParams.append(key, params[key]))
      fetch(
        url,
        {
          method: 'GET',
          headers: {
            'Accept': (
              'application/json, application/xml, ' +
              'text/plain, text/html, ' +
              '*.*'
            ),
            'Content-Type': 'application/json;charset=UTF-8',
            'apikey': payload.token
          },
          mode: 'cors'
        }
      )
        .then(validateFetchResponse)
        .then(data => {
          context.commit('SET_COMPANY_LIST', data.results)
          context.commit(
            'SET_COMPANY_LIST_LENGTH',
            data.metadata.pagination.total
          )
          return data
        })
        .catch(console.error)
    },
    GET_CONTACT_LIST: (context, payload) => {
      const url = new URL('https://api.moskitcrm.com/v1/contacts')
      const params = { sort: 'name', start: payload.start || 0 }
      Object.keys(params)
        .forEach(key => url.searchParams.append(key, params[key]))
      fetch(
        url,
        {
          method: 'GET',
          headers: {
            'Accept': (
              'application/json, application/xml, ' +
              'text/plain, text/html, ' +
              '*.*'
            ),
            'Content-Type': 'application/json;charset=UTF-8',
            'apikey': payload.token
          },
          mode: 'cors'
        }
      )
        .then(validateFetchResponse)
        .then(data => {
          context.commit('SET_CONTACT_LIST', data.results)
          context.commit(
            'SET_CONTACT_LIST_LENGTH',
            data.metadata.pagination.total
          )
          return data
        })
        .catch(console.error)
    }
  }
})
