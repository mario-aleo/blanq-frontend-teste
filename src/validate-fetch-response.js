export default function (response) {
  if (response.status !== 200) {
    throw new Error(`Server Status ${response.status}`)
  }
  return response.json()
}
