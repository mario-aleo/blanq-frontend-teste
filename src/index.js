import Vue from 'vue'
import App from './App.vue'
import store from './store'
import VueRouter from 'vue-router'
import VueMDCAdapter from 'vue-mdc-adapter'
import LoginComponent from './components/LoginComponent.vue'
import ContactsComponent from './components/ContactsComponent.vue'
import CompaniesComponent from './components/CompaniesComponent.vue'
import 'vue-mdc-adapter/dist/vue-mdc-adapter.css'
import '../assets/app.styl'

Vue.use(VueRouter)
Vue.use(VueMDCAdapter)

/* eslint-disable-next-line no-new */
new Vue({
  el: '#app',
  store,
  router: new VueRouter({
    routes: [
      { path: '*', redirect: '/login' },
      { path: '/login', component: LoginComponent },
      { path: '/contatos', component: ContactsComponent },
      { path: '/empresas', component: CompaniesComponent }
    ]
  }),
  render: h => h(App)
})
