module.exports = {
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'plugin:import/recommended',
    'plugin:promise/recommended',
    'plugin:vue/recommended',
    'standard'
  ],
  plugins: [
    'vue'
  ]
};